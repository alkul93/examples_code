<?php

namespace common\components\analytics\builders;

/**
 * Строитель. Подготавливат данные, на основе которых будем выводить таблицы и строить графики на фронте.
 */
class AnalyticBuilder
{

    // Состояния расчетов для проверки
    /** @var integer Ничего не расчитано */
    const NOT_ALL       = 1;
    /** @var integer Не расчитаны доходы */
    const NOT_INCOME    = 2;
    /** @var integer Не расчитаны расходы */
    const NOT_OUTCOME   = 3;

    /**
     * @var array Массив со значениями для собственных вагонов
     */
    protected $ownWagons = [];

    /**
     * @var array Массив со значениями для дочерних вагонов
     */
    protected $subWagons = [];

    /**
     * @var array Массив со значениями для арендованых вагонов
     */
    protected $rentedWagons = [];

    /**
     * @var array Массив со значениями для привлеченных вагонов
     */
    protected $attractedWagons = [];

    /**
     * @var array Общий доход
     */
    protected $totalCost = [];

    /**
     * @var array Массив с информацией об аналитики вагонов
     */
    protected $wagons;

    /**
     * Добавляем по одному вагону в билдер, и потом сформируем массив для вывода
     *
     * @param \common\models\wagons_array\Wagons $wagon Объект вагона
     * @param float $income Доход вагона
     * @param float $outcome Расходы вагона
     * @param boolean $nullWagon Показывать вагоны с нулевым пробегом
     */
    public function addWagon($wagon, $income, $outcome, $nullWagon = false)
    {
        if ($nullWagon == 'false') {
            if ($income != 0 || $outcome != 0) {
                $this->wagons[] = [
                    'id' => $wagon->id,
                    'number' => $wagon->number,
                    'owner' => $wagon->owner->name,
                    'income' => $income,
                    'outcome' => $outcome
                ];
            }
        } else {
            $this->wagons[] = [
                'id' => $wagon->id,
                'number' => $wagon->number,
                'owner' => $wagon->owner->name,
                'income' => $income,
                'outcome' => $outcome
            ];
        }


    }

    /**
     * @return array Массив вагонов
     */
    public function buildWagons() : array
    {
        return $this->wagons;
    }

    /**
     * Задаем значения для дочерних вагонов.
     * Где $type - Тип (расход, доход, кол-во)
     * $value - Значение параметра
     *
     * @param string $type Тип операции (расход ('outcome'), доход ('income'), кол-во ('count'))
     * @param float $value Значение
     * @return \common\components\analytics\builders\AnalyticBuilder;
     */
    public function setSubWagons(string $type, float $value) : AnalyticBuilder
    {
        $this->subWagons[$type] = $value;
        return $this;
    }

    /**
     * Задаем значения для собственных вагонов.
     * Где $type - Тип (расход, доход, кол-во)
     * $value - Значение параметра
     *
     * @param string $type Тип операции (расход ('outcome'), доход ('income'), кол-во ('count'))
     * @param float $value Значение
     * @return \common\components\analytics\builders\AnalyticBuilder;
     */
    public function setOwnWagons(string $type, float $value) : AnalyticBuilder
    {
        $this->ownWagons[$type] = $value;
        return $this;
    }

    /**
     * Задаем значения для арендованых вагонов.
     * Где $type - Тип (расход, доход, кол-во)
     * $value - Значение параметра
     *
     * @param string $type Тип операции (расход ('outcome'), доход ('income'), кол-во ('count'))
     * @param float $value Значение
     * @return \common\components\analytics\builders\AnalyticBuilder;
     */
    public function setRentedWagons(string $type, float $value) : AnalyticBuilder
    {
        $this->rentedWagons[$type] = $value;
        return $this;
    }

    /**
     * Задаем значения для привлеченных вагонов.
     * Где $type - Тип (расход, доход, кол-во)
     * $value - Значение параметра
     *
     * @param string $type Тип операции (расход ('outcome'), доход ('income'), кол-во ('count'))
     * @param float $value Значение
     * @return \common\components\analytics\builders\AnalyticBuilder;
     */
    public function setAttractedWagons(string $type, float $value) : AnalyticBuilder
    {
        $this->attractedWagons[$type] = $value;
        return $this;
    }

    /**
     * Задаем общие параметры (Общий доход, общий расход)
     *
     * @param string $type Тип операции (расход ('outcome'), доход ('income'), кол-во ('count'))
     * @param float $value Значение
     * @return \common\components\analytics\builders\AnalyticBuilder;
     */
    public function setTotalCost(string $type, float $value) : AnalyticBuilder
    {
        $this->totalCost[$type] = $value;
        return $this;
    }

    /**
     * Проверяем произведены ли расчеты для доходов/расходов
     *
     * @return integer
     */
    public function checkSums() : integer
    {
        if (empty($this->totalCost)) {
            return self::NOT_ALL;
        }

        if (!isset($this->totalCost['outcome'])) {
            return self::NOT_OUTCOME;
        }

        if (!isset($this->totalCost['income'])) {
            return self::NOT_INCOME;
        }

        return 0;
    }

    /**
     * Формируем детальную информацию об одном вагоне
     * В результате работы будет массив, отсортированый дате, где будут учитываться
     * все доходы и расходы
     *
     * @param array $array Массив с доходами и расходами
     * @return array
     */
    public function buildAdvancedWagonInfo(array $array) : array
    {
        return [
            'result' => $this->sort(array_merge($array['income'], $array['outcome'])),
            'total' => $array['incomeTotal'] - $array['outcomeTotal'],
        ];
    }

    /**
     * Построение общей аналитики по всем вагонам
     *
     * @return array
     */
    public function buildTotal() : array
    {
        return [
            'own' => [
                'income' => $this->ownWagons['income'],
                'outcome' => $this->ownWagons['outcome'],
                'count' => $this->ownWagons['count']
            ],
            'sub' => [
                'income' => $this->subWagons['income'],
                'outcome' => $this->subWagons['outcome'],
                'count' => $this->subWagons['count']
            ],
            'rented' => [
                'income' => $this->rentedWagons['income'],
                'outcome' => $this->rentedWagons['outcome'],
                'count' => $this->rentedWagons['count']
            ],
            'attracted' => [
                'income' => $this->attractedWagons['income'],
                'outcome' => $this->attractedWagons['outcome'],
                'count' => $this->attractedWagons['count']
            ],
            'total' => [
                'income' => $this->totalCost['income'],
                'outcome' => $this->totalCost['outcome'],
                'count' => $this->getTotalCountWagons()
            ]
        ];
    }

    /**
     * Получить общее количество вагонов
     *
     * @return integer
     */
    protected function getTotalCountWagons()
    {
        return $this->ownWagons['count'] + $this->subWagons['count'] + $this->rentedWagons['count'] + $this->attractedWagons['count'];
    }

    /**
     * Сортировка по дате
     *
     * @param array $array Массив для сортировки
     * @return array Отсортированный массив
     */
    public function sort(array $array) : array
    {
        usort($array, function ($v1, $v2)
        {
            if ($v1["date"] == $v2["date"]) return 0;
            return ($v1["date"] > $v2["date"])? -1: 1;
        });

        return $array;
    }
}
