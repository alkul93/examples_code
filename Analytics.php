<?php

namespace common\components\analytics;

use \common\components\analytics\generators\IncomeGenerator;
use \common\components\analytics\generators\OutcomeGenerator;
use \common\components\analytics\builders\AnalyticBuilder;
use \common\models\wagons_array\Wagons;

/**
 * Компонент для расчета аналитичиских данных
 */
class Analytics
{
    /** @var integer Собственные */
    const OWN       = 1;
    /** @var integer Арендованые */
    const RENTED    = 2;
    /** @var integer Привлеченые */
    const ATTRACTED = 3;
    /** @var integer Дочерние */
    const SUB       = 4;

    /**
     * @var \common\components\analytics\generators\IncomeGenerator;
     */
    protected $incomeGenerator = null;

    /**
     * @var \common\components\analytics\generators\OutcomeGenerator;
     */
    protected $outcomeGenerator = null;

    /**
     * @var string Дата начала периода
     */
    protected $dateStart;

    /**
     * @var string Дата окончания периода;
     */
    protected $dateEnd;

    /**
     * @var boolean Вагоны с нулевым доходом;
     */
    protected $nullWagon = false;

    /**
     * @var \common\components\analytics\builders\AnalyticBuilder
     */
    protected $builder;

    /**
     * @var \common\models\wagons_array\Wagons
     */
    protected $wagonModel;

    /**
     * В конструкторе создаем генераторы для расчета дохода и расхода, так же
     * формируем строитель для формирования ответа и создаем модель вагонов.
     *
     */
    public function __construct()
    {
        $this->incomeGenerator = IncomeGenerator::getInstance();
        $this->outcomeGenerator = OutcomeGenerator::getInstance();
        $this->builder = new AnalyticBuilder;
        $this->wagonModel = new Wagons;
    }

    /**
     * Задаем период за который будем создавать аналитику. По умолчанию одни сутки.
     *
     * @param integer $dateStart Дата начала периода в timestamp
     * @param integer $dateEnd Дата окончания периода в timestamp
     * @return \common\components\analytics\Analytics
     */
    public function setPeriod(int $dateStart, int $dateEnd) : Analytics
    {
        $this->dateStart = $dateStart;
        $this->dateEnd = $dateEnd + 86400;

        $this->incomeGenerator->setParams([
            'dateStart' => $this->dateStart,
            'dateEnd' => $this->dateEnd
        ]);

        $this->outcomeGenerator->setParams([
            'dateStart' => $this->dateStart,
            'dateEnd' => $this->dateEnd
        ]);

        return $this;
    }

    /**
     * Показывать ли вагоны с нулевыми доходами и раходами
     *
     * @param integer $value
     * @return \common\components\analytics\Analytics
     */
    public function setNullWagon($value) : Analytics
    {
        $this->nullWagon = $value;
        return $this;
    }

    /**
     * Получаем общий доход по всем вагонам.
     *
     * @return float $totalCost Общая сумма доходов за выбраный период
     */
    public function getIncome() : float
    {
        $own = $this->getSum(self::OWN, 'setOwnWagons');
        $rented = $this->getSum(self::RENTED, 'setRentedWagons');
        $attracted = $this->getSum(self::ATTRACTED, 'setAttractedWagons');
        $sub = $this->getSum(self::SUB, 'setSubWagons');

        $totalCost = $own + $rented + $attracted + $sub;

        $this->builder->setTotalCost('income', $totalCost);
        return $totalCost;
    }

    /**
     * Получаем общий расход по всем вагонам за выбраный период
     *
     * @param void
     * @return float $totalCost Общая сумма расходов за выбраный период
     */
    public function getOutcome()
    {
        $own = $this->getOutSum(self::OWN, 'setOwnWagons');
        $rented = $this->getOutSum(self::RENTED, 'setRentedWagons');
        $attracted = $this->getOutSum(self::ATTRACTED, 'setAttractedWagons');
        $sub = $this->getOutSum(self::SUB, 'setSubWagons');

        $totalCost = $own + $rented + $attracted + $sub;

        $this->builder->setTotalCost('outcome', $totalCost);
        return $totalCost;
    }

    public function build()
    {
        switch ($this->builder->checkSums()) {
            case $this->builder::NOT_ALL:
                $this->getOutcome();
                $this->getIncome();
                break;

            case $this->builder::NOT_INCOME:
                $this->getIncome();
                break;

            case $this->builder::NOT_OUTCOME:
                $this->getOutcome();
                break;

            default:

                break;
        }
        return $this->builder->buildTotal();
    }

    /**
     * Получение аналитики по всем вагонам в отдельности. Т.е. для каждого вагона
     * будет сформированы доходы, расходы, за выбраный месяц, и за предыдущий для
     * сравнения.
     *
     * @param integer $type Тип принадлежности парка для которого будем формировать отчет
     * @return array
     */
    public function buildAdvancedAnalytic($type = self::OWN)
    {
        $wagons = $this->wagonModel::find()->where(['belong_wagon_id' => $type])->all();

        foreach ($wagons as $wagon) {
            $this->builder->addWagon(
                $wagon,
                $this->incomeGenerator->getWagonInfo($wagon),
                $this->outcomeGenerator->getWagonInfo($wagon),
                $this->nullWagon
            );
        }

        return $this->builder->buildWagons();
    }

    /**
     * Построение аналитика для конкретного вагона. Результатом будет таблица со
     * всеми расходами и доходами
     *
     * @param integer $id Идентификатор вагона
     * @return array Массив со всеми перевозками, отсортироваными по дате
     */
    public function buildOneWagonAnalytic($id)
    {
        $wagon = $this->wagonModel::findOne($id);

        $result = [
            'income'        =>  $this->incomeGenerator->getAdvancedWagonInfo($wagon),
            'incomeTotal'   =>  $this->incomeGenerator->getWagonInfo($wagon),
            'outcome'       =>  $this->outcomeGenerator->getAdvancedWagonInfo($wagon),
            'outcomeTotal'  =>  $this->outcomeGenerator->getWagonInfo($wagon),
        ];

        return $this->builder->buildAdvancedWagonInfo($result);
    }

    /**
     * Получаем сумму для определенного типа вагона, и записываем ее в билдер
     *
     * @param integer $wagonOwn Тип вагона (собственный, арендованый etc.)
     * @param string $builderMethod Название метода в билдере, который будет запиывать
     *
     * @return integer $cost Сумма дохода этого типа вагонов
     */
    protected function getSum($wagonOwn, $builderMethod)
    {
        $wagons = $this->wagonModel::find()->where(['belong_wagon_id' => $wagonOwn]);
        $clone = clone $wagons;
        $count = $clone->count();

        $cost = $this->incomeGenerator->setWagons($wagons->all())->calculate();

        $this->builder->$builderMethod('income', $cost);
        $this->builder->$builderMethod('count', $count);

        return $cost;
    }

    /**
     * Получаем сумму для определенного типа вагона, и записываем ее в билдер
     *
     * @param integer $wagonOwn Тип вагона (собственный, арендованый etc.)
     * @param string $builderMethod Название метода в билдере, который будет запиывать
     *
     * @return integer $cost Сумма дохода этого типа вагонов
     */
    protected function getOutSum($wagonOwn, $builderMethod)
    {
        $wagons = $this->wagonModel::find()->where(['belong_wagon_id' => $wagonOwn]);
        $cost = $this->outcomeGenerator->setWagons($wagons->all())->calculate();

        $this->builder->$builderMethod('outcome', $cost);


        return $cost;
    }
}
