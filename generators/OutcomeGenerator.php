<?php

namespace common\components\analytics\generators;

use common\models\analytics\Els;
use common\models\analytics\AdditionalDues;
use common\models\analytics\AdditionalServices;


/**
 * Класс для расчета расходов
 */
class OutcomeGenerator
{

    /**
     * @var \common\components\analytics\generators\OutcomeGenerator
     */
    protected static $instance = null;

    /**
     * @var \common\models\analytics\Els Модель ЕЛС (порожний пробег)
     */
    protected $elsModel = null;

    /**
     * @var \common\models\analytics\AdditionalDues Модель Дополнительных сборов
     */
    protected $duesModel = null;

    /**
     * @var \common\models\analytics\AdditionalServices Модель доп. услуг
     */
    protected $serviceModel = null;

    /**
     * @var array Параметры по которым будет производить расчет расходов.
     */
    protected $params;

    /**
     * @var array Массив вагонов
     */
    protected $wagons;

    /**
     * @var array Массив иденитификаторов вагонов
     */
    protected $wagonIds;

    /**
     * В конструкторе создаются объекты моделей расчета расхода
     *
     * @param void
     */
    protected function __construct()
    {
        $this->elsModel = new Els;
        $this->duesModel = new AdditionalDues;
        $this->serviceModel = new AdditionalServices;
    }

    /**
     * Получение экземпляра класса
     *
     * @param void
     * @return \common\components\analytics\generators\OutcomeGenerator;
     */
    public static function getInstance() : OutcomeGenerator
    {
        return self::$instance === null ? self::$instance = new static() : self::$instance;
    }


    /**
     * Задаем номер вагона, по которому будет проводить расчет
     *
     * @param array $params Параметры по которым будет производить расчет расходов.
     * Массив должен содеражать следующие ключи:
     * 'dateStart' - Дата начала периода
     * 'dateEnd' - Дата окончания расчетного периода
     *
     * @return \common\components\analytics\generators\OutcomeGenerator;
     */
    public function setParams(array $params) : OutcomeGenerator
    {
        $this->params = $params;
        return $this;
    }

    /**
     * Задаем список вагонов для которых нужно посчитать расход
     *
     * @param array $wagons Массив вагонов
     * @return \common\components\analytics\generators\OutcomeGenerator;
     */
    public function setWagons($wagons) : OutcomeGenerator
    {
        $this->wagons = $wagons;
        $this->wagonIds = [];

        if (!empty($wagons)) {
            foreach ($wagons as $wagon) {
                $this->wagonIds[] = $wagon->id;
            }
        }

        return $this;
    }

    /**
     * Производим расчет общего расхода для выбраных вагонов за выбраный период
     *
     * @param string $field Поле, по которому сумировать результаты
     *
     * @return float Сумма расхода
     */
    public function calculate($field = 'sum')
    {
        $cost = 0;

        if (empty($this->wagonIds)) {
            return $cost;
        }

        $cost = $this->getOutcomeEls($field);
        $cost += $this->getOutcomeDues($field);
        $cost += $this->getOutcomeServices($field);

        return $cost;
    }

    /**
     * Получаем расход для одного вагона
     *
     * @param \common\models\wagons_array\Wagons $wagon Объект вагона
     * @param string $field Поле по которому суммировать данные из таблицы трафика
     * @return float/integer
     */
    public function getWagonInfo($wagon, $field = 'sum')
    {
        $cost = $this->elsModel::find()->where(['wagon_id' => $wagon->id])
                    ->andWhere(['>', 'ticket_date', $this->params['dateStart']])
                    ->andWhere(['<', 'ticket_date', $this->params['dateEnd']])
                    ->sum($field);

        $cost += $this->duesModel::find()->where(['wagon_id' => $wagon->id])
                    ->andWhere(['>', 'ticket_date', $this->params['dateStart']])
                    ->andWhere(['<', 'ticket_date', $this->params['dateEnd']])
                    ->sum($field);

        $cost += $this->serviceModel::find()->where(['wagon_id' => $wagon->id])
                    ->andWhere(['>', 'ticket_date', $this->params['dateStart']])
                    ->andWhere(['<', 'ticket_date', $this->params['dateEnd']])
                    ->sum($field);

        return $cost ?? 0;
    }

    /**
     * Получение перевозок для конкретного вагона
     *
     * @param \common\models\wagons_array\Wagons $wagon Объект вагона
     * @return array Массив объектов всех перевозок
     */
    public function getAdvancedWagonInfo($wagon)
    {
        $result = [];
        $elsModel = clone $this->elsModel;
        $duesModel = clone $this->duesModel;
        $serviceModel = clone $this->serviceModel;

        $resultQuery = [
            'els'       => $elsModel::find()->where(['wagon_id' => $wagon->id])
                            ->andWhere(['>', 'ticket_date', $this->params['dateStart']])
                            ->andWhere(['<', 'ticket_date', $this->params['dateEnd']])
                            ->all(),
            'dues'      => $duesModel::find()->where(['wagon_id' => $wagon->id])
                            ->andWhere(['>', 'ticket_date', $this->params['dateStart']])
                            ->andWhere(['<', 'ticket_date', $this->params['dateEnd']])
                            ->all(),
            'service'   => $serviceModel::find()->where(['wagon_id' => $wagon->id])
                            ->andWhere(['>', 'ticket_date', $this->params['dateStart']])
                            ->andWhere(['<', 'ticket_date', $this->params['dateEnd']])
                            ->all(),
        ];


        /**
         * @var \common\models\analytics\Els $item
         */
        foreach ($resultQuery['els'] as $item) {
            $result[] = [
                'wagon' => $wagon,
                'station_from' => $item->getStationFrom()->one(),
                'station_to' => $item->getStationTo()->one(),
                'date' => $item->ticket_date,
                'client' => $item->getClient()->one(),
                'sum' => $item->sum,
                'text' => 'Порожний пробег',
                'type' => 'outcome',
            ];
        }

        /**
         * @var \common\models\analytics\AdditionalDues $item
         */
        foreach ($resultQuery['dues'] as $item) {
            $result[] = [
                'wagon' => $wagon,
                'station_from' => $item->getStationFrom()->one(),
                'station_to' => $item->getStationTo()->one(),
                'date' => $item->ticket_date,
                'client' => $item->getClient()->one(),
                'sum' => $item->sum,
                'text' => 'Доп. сборы',
                'type' => 'outcome',
            ];
        }

        /**
         * @var \common\models\analytics\AdditionalServices $item
         */
        foreach ($resultQuery['service'] as $item) {
            $result[] = [
                'wagon' => $wagon,
                'date' => $item->ticket_date,
                'sum' => $item->sum,
                'text' => 'Доп. услуги',
                'type' => 'outcome',
            ];
        }

        return $result;

    }

    /**
     * Находим сумму расходов по таблице порожнего пробега
     *
     * @param string $field Поле по которому суммирует резльтат
     * @return float Сумма
     */
    protected function getOutcomeEls($field) : float
    {
        $model = clone $this->elsModel;
        $result = $model::find()->where(['wagon_id' => $this->wagonIds])
                    ->andWhere(['>', 'ticket_date', $this->params['dateStart']])
                    ->andWhere(['<', 'ticket_date', $this->params['dateEnd']])
                    ->sum($field);

        if (!$result) {
            return 0;
        }

        return $result;
    }

    /**
     * Находим сумму расходов по таблице дополнительных сборов
     *
     * @param string $field Поле по которому суммирует резльтат
     * @return float Сумма
     */
    protected function getOutcomeDues($field) : float
    {
        $model = clone $this->duesModel;
        $result = $model::find()->where(['wagon_id' => $this->wagonIds])
                    ->andWhere(['>', 'ticket_date', $this->params['dateStart']])
                    ->andWhere(['<', 'ticket_date', $this->params['dateEnd']])
                    ->sum($field);

        if (!$result) {
            return 0;
        }

        return $result;
    }

    /**
     * Находим сумму расходов по таблице дополнительных услуг
     *
     * @param string $field Поле по которому суммирует резльтат
     * @return float Сумма
     */
    protected function getOutcomeServices($field) : float
    {
        $model = clone $this->serviceModel;

        $result = $model::find()->where(['wagon_id' => $this->wagonIds])
                    ->andWhere(['>', 'ticket_date', $this->params['dateStart']])
                    ->andWhere(['<', 'ticket_date', $this->params['dateEnd']])
                    ->sum($field);

        if (!$result) {
            return 0;
        }

        return $result;
    }
}
