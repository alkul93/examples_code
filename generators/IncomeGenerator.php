<?php

namespace common\components\analytics\generators;

use common\models\wagons_array\DislocationLast;
use common\models\wagons_array\Traffic;
use common\models\wagons_array\Wagons;

/**
 * Класс для расчета доходов
 */
class IncomeGenerator
{
    /**
     * @var \common\components\analytics\generators\IncomeGenerator
     */
    protected static $instance = null;

    /**
     * @var array Параметры по которым будет производить расчет доходов.
     */
    protected $params;

    /**
     * @var array Массив вагонов
     */
    protected $wagons;

    /**
     * @var array Массив идентификаторов вагонов
     */
    protected $wagonIds;

    /**
     * @var \common\models\wagons_array\Traffic
     */
    protected $model;

    protected function __construct()
    {
        $this->model = new Traffic;
    }

    /**
     * Получение экземпляра класса
     *
     * @return \common\components\analytics\generators\IncomeGenerator
     */
    public static function getInstance() : IncomeGenerator
    {
        return self::$instance === null ? self::$instance = new static() : self::$instance;
    }

    /**
     * Задаем номер вагона, по которому будет проводить расчет
     *
     * @param array $params Параметры по которым будет производить расчет доходов.
     * Массив должен содеражать следующие ключи:
     * 'dateStart' - Дата начала периода
     * 'dateEnd' - Дата окончания расчетного периода
     *
     * @return \common\components\analytics\generators\IncomeGenerator;
     */
    public function setParams(array $params) : IncomeGenerator
    {
        $this->params = $params;
        return $this;
    }

    /**
     * Задаем список вагонов для которых нужно посчитать доход
     *
     * @param array $wagons Массив вагонов
     * @return \common\components\analytics\generators\IncomeGenerator;
     */
    public function setWagons($wagons)
    {
        $this->wagons = $wagons;
        $this->wagonIds = [];

        if (!empty($wagons)) {
            foreach ($wagons as $wagon) {
                $this->wagonIds[] = $wagon->id;
            }
        }

        return $this;
    }

    /**
     * Получение перевозок для конкретного вагона
     *
     * @param \common\models\wagons_array\Wagons $wagon Объект вагона
     * @return array Массив объектов всех перевозок
     */
    public function getAdvancedWagonInfo($wagon)
    {
        $traffic = clone $this->model;

        $models = $traffic::find()->joinWith([
            'dislocation' => function ($q) use ($wagon)
            {
                /**
                 * @var DislocationLast $q;
                 */
                $q->where(['wagon_id' => $wagon->id])
                    ->andWhere(['>', 'delivery_date', $this->params['dateStart']])
                    ->andWhere(['<', 'delivery_date', $this->params['dateEnd']]);
            }
        ])->all();

        $result = [];

        foreach ($models as $model) {
            $result[] = [
                'wagon' => $wagon,
                'station_from' => $model->dislocation->stationFrom,
                'station_to' => $model->dislocation->stationTo,
                'date' => $model->dislocation->delivery_date,
                'client' => $model->contragent,
                'sum' => $model->price,
                'text' => 'Доход',
                'type' => 'income'
            ];
        }

        return $result;
    }

    /**
     * Получение дохода отдельного вагона
     *
     * @param \common\models\wagons_array\Wagons $wagon Объект вагона
     * @param string $field Поле по которому суммировать данные из таблицы трафика
     * @return float/integer
     */
    public function getWagonInfo(Wagons $wagon, string $field = 'price')
    {
        $sum = $this->model::find()->joinWith([
            'dislocation' => function ($q) use ($wagon)
            {
                $q->where(['wagon_id' => $wagon->id])
                    ->andWhere(['>', 'delivery_date', $this->params['dateStart']])
                    ->andWhere(['<', 'delivery_date', $this->params['dateEnd']]);
            }
        ])->sum($field);

        return $sum ?? 0;
    }

    /**
     * Производим расчет доход вагона за выбраный период
     *
     * @param string $field Поле, по которому считаем сумму
     * @return float Сумма дохода
     */
    public function calculate(string $field = 'price')
    {
        if (empty($this->wagonIds)) {
            return 0;
        }

        $sum = $this->model::find()->joinWith([
            'dislocation' => function ($q)
            {
                $q->where(['wagon_id' => $this->wagonIds])
                    ->andWhere(['>', 'delivery_date', $this->params['dateStart']])
                    ->andWhere(['<', 'delivery_date', $this->params['dateEnd']]);
            }
        ])->sum($field);

        return $sum ?? 0;
    }
}
